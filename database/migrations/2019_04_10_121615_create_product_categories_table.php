<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {

            $table->string('product_id', 64)->index();

            $table->string('category_id', 64)->index();

            $table->foreign('product_id')->references('p_id')->on('products')->onDelete('cascade');

            $table->foreign('category_id')->references('c_id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_categories', function (Blueprint $table) {

            $table->dropForeign(['product_id']);

            $table->dropForeign(['category_id']);
        });


    }
}
