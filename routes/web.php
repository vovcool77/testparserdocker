<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ParserController@index')->middleware('auth');

Route::post('/', 'ParserController@add');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/categories/products/{id}', 'ProductController@show')->name('categories.product');
Route::get('/categories/products/{id}/edit', 'ProductController@edit')->name('categories.editor');
Route::resource('categories', 'CategoryController')
    ->names('categories')->middleware('auth');

Route::resource('products', 'ProductController')
    ->names('products')->middleware('auth');
