<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Category;


class CategoryController extends Controller
{
    /**
     * Show all categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = new Category();

        $paginator = $item->paginate(5);

        return view('categories.index')
            ->with(compact('paginator'));
    }


    /**
     * Show products, which are related to category.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = new Category();

        $getCategory = $item->getCategory($id)->paginate(5);

        return view('categories.show')
            ->with(compact('getCategory'));

    }

    public function edit($id)
    {
        $item = new Product();

        $product = $item->getProduct($id);

        $image = $item->getImage($id);

        return view('categories.edit')
            ->with(compact('product'))
            ->with(compact('image'));
    }

    /**
     * Updating product.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = new Product();

        $productUpd = $item->updateProduct(
            $id,
            $request->p_name,
            $request->description,
            $request->specification,
            $request->price
        );

        if ($productUpd){
            return redirect()
                ->route('categories.edit', $id)
                ->with(['success' => 'Збережено успішно!']);
        } else {
            return back()
                ->withErrors(['msg' => "Помилка збереження!"])
                ->withInput();
        }
    }

    /**
     * Deleting product.
     *
     * @param  int  $id
     * @return \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Product $product)
    {
        $result = $product->productDelete($id);

        if ($result){
            return redirect()
                ->route('categories.index')
                ->with(['success' => "Товар з ідентифікатором [$id] був видалений!"]);
        } else {
            return back()
                ->withErrors(['msg' => "Помилка видалення!"])
                ->withInput();
        }
    }
}
