<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Facades\Config;

class Parser extends Model
{

    protected $categories = [];

    /**
     * @param $url
     * @return array|mixed
     */
    protected function getPage($url)
    {
        $page = curl_init();

        curl_setopt($page, CURLOPT_URL, $url);

        curl_setopt($page, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($page, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($page, CURLOPT_HEADER, true);

        $result = curl_exec($page) ?? [];

        return $result;
    }

    /**
     * Get links to product pages.
     * @param $page
     * @return array
     */
    protected function getPageLinks($page)
    {
        $crawler = new Crawler($page);

        $result = $crawler->filterXPath("//div[@class = 'g-i-tile-i-title clearfix']/a")->extract(array('href')) ?? [];

        return $result;
    }

    /**
     * @param $title
     * @return null|string
     */
    protected function getTitle($title)
    {
        $crawler = new Crawler($title);
        try {
            $product['title'] = $crawler->filterXpath("//meta[@name = 'keywords']")->attr('content');
        } catch (\Exception $e) {
            $product['title'] = '';
        }

        return $product['title'];
    }

    /**
     * @param $description
     * @return string
     */
    protected function getDescription($description)
    {
        $crawler = new Crawler($description);
        try {
            $product['description'] = $crawler->filterXpath("//div[@class = 'b-rich-text goods-description-content']")->text();
        } catch (\Exception $e) {
            $product['description'] = '';
        }

        return $product['description'];
    }

    /**
     * @param $specification
     * @return string
     */
    protected function getSpecification($specification)
    {
        $crawler = new Crawler($specification);
        try {
            $product['specification'] = $crawler->filterXpath("//div[@class = 'short-description ng-star-inserted']")->html();
        } catch (\Exception $e) {
            $product['specification'] = '';
        }

        return $product['specification'];
    }

    /**
     * @param $price
     * @return int|null|string
     */
    protected function getPrice($price)
    {
        $crawler = new Crawler($price);
        try {
            $product['price'] = $crawler->filterXpath("//meta[@itemprop = 'price']")->attr('content');
        } catch (\Exception $e) {
            $product['price'] = 0;
        }

        return $product['price'];
    }

    /**
     * @param $images
     * @return array|string
     */
    protected function getImages($images)
    {
        $crawler = new Crawler($images);
        try {
            $product['images'] = $crawler->filterXPath("//div[@class = 'pp-photo-l clearfix']/a")->extract(array('href'));
        } catch (\Exception $e) {
            $product['images'] = '';
        }

        return $product['images'];
    }

    /**
     * @param $categories
     * @param $productID
     * @return array
     */
    protected function getCategories($categories, $productID)
    {
        $crawler = new Crawler($categories);
        try {
            $product['categories'] = $crawler->filterXPath("//span[@class = 'breadcrumbs-title ng-star-inserted']")->extract(array('_text'));
        } catch (\Exception $e) {
            $product['categories'] = '';
        }

        $categories = [];

        foreach ($product['categories'] as $cname) {

            $categories[$cname] = $productID;

            $this->categories[$cname] = uniqid();
        }

        return $categories;
    }


    /**
     * Method use previous methods and output array of products.
     * @param $url
     * @param $numberOfPages
     * @return array
     */
    protected function getProductsByUrl($url, $numberOfPages)
    {
        $products = [];

        for ($i = 1; $i <= $numberOfPages; $i++) {

            sleep(1);

            $page = $this->getPage(rtrim($url, '/') . '/page=' . $i . '/');

            $productPageLinks = $this->getPageLinks($page);

            foreach ($productPageLinks as $productPageLink) {

                $productPage = $this->getPage($productPageLink);

                $product = [
                    'id' => uniqid(),
                    'title' => '',
                    'description' => '',
                    'specification' => '',
                    'price' => 0,
                    'images' => [],
                    'categories' => [],
                ];

                $product['title'] = $this->getTitle($productPage);
                $product['description'] = $this->getDescription($productPage);
                $product['specification'] = $this->getSpecification($productPage);
                $product['price'] = $this->getPrice($productPage);
                $product['images'] = $this->getImages($productPage);
                $product['categories'] = $this->getCategories($productPage, $product['id']);

                $products[] = $product;
            }
        }

        return $products;
    }

    /** Method returns array of categories.
     * @return array
     */
    protected function getCategoriesEntity()
    {
        return $this->categories;
    }

    /** Method get`s link and number of pages,
     *  use arrays of products & categories
     *  and save`s them into DB.
     * @param $link
     * @param $numbersOfPages
     */
    public function parserStart($link, $numbersOfPages)
    {

        $category = new Category();
        $product = new Product();

        $url = $link ?? Config::get('settings.url_to_parse');
        $page = $numbersOfPages ?? Config::get('settings.number_of_pages');

        $products = $this->getProductsByUrl($url, $page);
        $categories = $this->getCategoriesEntity();

        $category->saveCategories($categories);
        $product->saveProdcuts($products, $categories);

    }
}

