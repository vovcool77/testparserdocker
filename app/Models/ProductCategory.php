<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductCategory
 *
 * @package App\Models
 *
 * @property integer                   $product_id
 * @property integer                   $category_id'
 */

class ProductCategory extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable
        = [
            'product_id',
            'category_id',
        ];
}