<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Images
 *
 * @package App\Models
 *
 * @property string                    $product_id
 * @property string                    $img_url
 */

class Image extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable
        = [
            'product_id',
            'img_url',
        ];


    /**
     * Image & Product relations.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'p_id', 'product_id');
    }
}