<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;


class ParserTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * This test check if parse page
     * loads ok.
     *
     * @return void
     */
    public function testMainPage()
    {
        $response = $this->get('/')
            ->assertSee('Відправити');
        $response->assertStatus(200);
    }
}
