@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('categories.includes.result_messages')
                <div class="card">
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <td># Категорії</td>
                                <td>Назва категорії</td>
                            </tr>
                            </thead>
                            <tbody>
                            {{--List of categories--}}
                            @foreach($paginator as $category)
                                @php /** @var \App\Models\Category $category */
                                @endphp
                                <tr>
                                    <td>{{ $category->c_id }}</td>
                                    <td>
                                        <a href="{{ route("categories.show", $category->c_id) }}">
                                            {{ $category->c_name }}</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{--Pagination--}}
        @if($paginator->total() > $paginator->count())
            <br>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {{ $paginator->links() }}
                            {{ csrf_field() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <br>
    </div>
@endsection