@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title"></div>
                        <ul class="nav nav-tabs">
                            <li class="navbar-brand">
                                <a class="nav-link mx-auto" data-toggle="tab" href="#maindata" role="navigation">Основна
                                    інформація</a>
                            </li>
                        </ul>
                        <br>
                        <div class="container">
                            <div class="tab-content">
                                <div class="tab-pane active" id="maindata" role="tabpanel">
                                    <div class="card">
                                        <p class="font-weight-bold">
                                            Назва : <a href={{route("categories.editor", $product->p_id)}}>
                                                {{ $product->p_name }}
                                            </a>
                                        </p>
                                    </div>
                                    <br>
                                    <div class="card">
                                        <p class="font-weight-bold">
                                            Опис : <a href={{route("categories.editor", $product->p_id)}}>
                                                {{ strip_tags($product->description) }}
                                            </a>
                                        </p>
                                    </div>
                                    <br>
                                    <div class="card">
                                        <p class="font-weight-bold">
                                            Специфікація : <a href={{route("categories.editor", $product->p_id)}}>
                                                {{ strip_tags($product->specification) }}
                                            </a>
                                        </p>
                                    </div>
                                    <br>
                                    <div class="card">
                                        <p class="font-weight-bold">
                                            Ціна : <a href="{{route("categories.editor", $product->p_id)}}">
                                                {{ $product->price }}
                                            </a> грн.
                                        </p>
                                    </div>
                                    <br>
                                    <div class="card">
                                        @foreach ($image as $img)
                                                {{ $img->img_url }}
                                            <br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection