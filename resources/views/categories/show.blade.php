@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>Назва</td>
                                <td>Специфікація</td>
                                <td>Ціна</td>
                            </tr>
                            </thead>
                            <tbody>
                            {{--List of products--}}
                            @foreach($getCategory as $category)
                                @php /** @var \App\Models\Category $category */
                                @endphp
                                <tr>
                                    <td>{{ $category->p_id }}</td>
                                    <td>
                                        <a href="{{ route("categories.product", $category->p_id) }}">
                                            {{ strip_tags($category->p_name) }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route("categories.product", $category->p_id) }}">
                                            {{ strip_tags($category->specification) }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route("categories.product", $category->p_id) }}">
                                            {{ $category->price }} грн.
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{--Pagination--}}
        @if($getCategory->total() > $getCategory->count())
            <br>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {{ $getCategory->links() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
