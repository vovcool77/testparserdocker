@php
    /** @var \App\Models\Product $item */
@endphp
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('products.update', $product->p_id) }}">
                    @method('PATCH')
                    @csrf
                    <button type="submit" class="btn btn-success">Зберегти</button>
                </form>
            </div>
        </div>
    </div>
</div>
<br>

<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('products.destroy', $product->p_id) }}">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Видалити</button>
                </form>
            </div>
        </div>
    </div>
</div>


