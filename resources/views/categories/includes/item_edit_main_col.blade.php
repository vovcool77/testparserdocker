@php
    /** @var \App\Models\Product $item */
@endphp
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#maindata" role="tab">Основна інформація</a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div class="tab-pane active" id="maindata" role="tabpanel">
                        <div class="form-group">
                            <label for="p_name">Назва :</label>
                            <input name="p_name" value="{{ $product->p_name }}"
                                   id="p_name"
                                   type="text"
                                   class="form-control"
                                   minlength="3"
                                   required>

                        </div>

                        <div class="form-group">
                            <label for="description">Опис :</label>
                            <textarea name="description"
                                      id="description"
                                      class="form-control"
                                      rows="3">{{ old('description', strip_tags($product->description)) }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="specification">Специфікація :</label>
                            <textarea name="specification"
                                      id="specification"
                                      class="form-control"
                                      rows="3">{{ old('description', strip_tags($product->specification)) }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="slug">Ціна :</label>
                            <input name="price" value="{{ $product->price }}"
                                   id="price"
                                   type="number"
                                   class="form-control">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
